(in-package :netfarm-tests)

(defclass noisy-interactive (interactive plain)
  ())

(defun run-tests (&optional interactive?)
  (eql (status
	(test
         '(parser-tests renderer-tests key-tests benchmark)
         :report (if interactive? 'noisy-interactive 'plain)))
       :passed))

(defun gitlab-ci-test ()
  (or (run-tests)
      (uiop:quit -1)))
