(in-package :netfarm-tests)

(define-test renderer-tests)

(defclass mock-named-netfarm-class (netfarm-class)
  ((name :initarg :netfarm-name :reader mock-name)))

(defmethod netfarm-class-name ((class mock-named-netfarm-class))
  (first (mock-name class)))

(defclass foobar ()
  ((foo :initarg :foo)
   (bar :initarg :bar))
  (:metaclass mock-named-netfarm-class)
  (:netfarm-name "test@foobar"))

(define-test slot-order
  :parent renderer-tests
  (is string= "---
\"schema\" \"test@foobar\"
---
\"bar\" \"baz\"
\"foo\" \"bar\"
"
      (render-object
       (make-instance 'foobar
		      :foo "bar"
		      :bar "baz"))
      "Netfarm standard ordering wasn't followed while rendering an instance
of *foobar-schema*"))

(define-test render-atoms
  :parent renderer-tests
  (is string= "\"Hello world!\"" (render "Hello world!")
      "An atom containing a space was not stringified")
  (is string= "integer:42" (render 42)
      "An integer was not prefixed with the integer: codec")
  (is string= "base64-data:AQID" (render #(1 2 3))
      "A vector was not converted to base64-data"))
