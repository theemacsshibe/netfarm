(in-package :netfarm-tests)

(define-test script-machine-tests)

(netfarm-scripts:define-script *two-plus-two* ()
  (:procedure 0) (byte 2) dup + return)

(netfarm-scripts:define-script *also-fairly-basic-maths* ()
  (:procedure 0) (byte 2) (byte 3) < return)

(defmacro should-return ((script-name &rest arguments) &rest values)
  `(is equal-dwim 
       ',values
       (netfarm-scripts:run-interpreter
        (netfarm-scripts:setup-interpreter ,script-name (list ,@arguments)))))

(define-test quick-maths
  :parent script-machine-tests
  (should-return (*two-plus-two*) 4)
  (should-return (*also-fairly-basic-maths*) :true))

(netfarm-scripts:define-script *get-the-foo* ("foo")
  (:procedure 1) (get-env 0 0) (get-value 0) object-value return)
(defclass thing-with-foo ()
  ((foo :initarg :foo))
  (:metaclass netfarm:netfarm-class))

(define-test object-stuff
  :parent script-machine-tests
  (should-return (*get-the-foo* (make-instance 'thing-with-foo :foo "This is a foo"))
                 "This is a foo"))
