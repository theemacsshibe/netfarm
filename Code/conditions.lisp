(in-package :netfarm)

(define-condition squirty-bottle (error)
  ((reason :initarg :reason :reader squirty-bottle-reason))
  (:report (lambda (c s)
             (format s "~&Bad user! You did something very, very bad:~%~2t~a"
                     (squirty-bottle-reason c)))))

(defun squirty-bottle (reason-control &rest reason-arguments)
  (error 'squirty-bottle
         :reason (apply #'format nil reason-control reason-arguments)))
