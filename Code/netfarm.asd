(asdf:defsystem :netfarm
  :author "Cooperative of Applied Language"
  :depends-on (:alexandria :babel :closer-mop :flexi-streams
               :ironclad :s-base64 :split-sequence
               :trivial-garbage :bordeaux-threads)
  :license "Cooperative Software License v1+"
  :version "0.1.0"
  :components ((:file "package")
               (:file "conditions" :depends-on ("package"))
               (:file "macros")
               (:module "Codecs"
                :depends-on ("package" "macros")
                :serial t
                :components ((:file "base64")
                             (:file "codecs")))
               (:module "Objects"
                :depends-on ("package" "macros")
                :serial t
                :components ((:file "hash")
                             (:module "MOP"
                              :components ((:file "rewrite-references")
                                           (:file "netfarm-class")
                                           (:file "netfarm-slot")))
                             (:file "objects")
                             (:file "type-specifiers")
                             (:file "hash-classes")
                             (:file "inbuilt-objects")
                             (:file "schema")
                             (:file "computed-values")
                             (:file "deep-copy")
                             (:file "inbuilt-schemas")))
               (:module "Scripts"
                :depends-on ("Objects")
                :serial t
                :components ((:file "package")
                             (:module "Script-machine"
                              :components ((:file "conditions")
                                           (:file "opcode-information")
                                           (:file "discriminator-function")
                                           (:file "script-machine")
                                           (:file "environments")
                                           (:file "control-flow")
                                           (:file "operators")
                                           (:file "forthisms")
                                           (:file "objects")
                                           (:file "assemble")
                                           (:file "define-script")))
                             (:module "Reference-side-effects"
                              :components ((:file "apply-side-effect")
                                           (:file "run-scripts")))))
               (:module "Text-format"
                :depends-on ("Codecs" "Objects")
                :components ((:file "new-parser")
                             (:file "new-renderer")))
               (:module "Binary-format"
                :depends-on ("Objects")
                :components ((:file "fasterer-output")
                             (:file "parser")
                             (:file "renderer" :depends-on ("fasterer-output"))))
               (:module "Crypto"
                :depends-on ("Binary-format")
                :serial t
                :components ((:file "hash")
                             (:file "keys")))))
