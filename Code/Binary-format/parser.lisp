(in-package :netfarm)

(defun read-integer (function)
  (let ((byte-count (funcall function))
        (accumulator 0))
    (dotimes (n byte-count)
      (setf accumulator
            (logior (funcall function)
                    (ash accumulator 8))))
    accumulator))

(defun read-byte-vector (function count)
  (let ((vector (make-array count :element-type '(unsigned-byte 8))))
    (dotimes (n count)
      (setf (aref vector n) (funcall function)))
    vector))

(defun read-string (function)
  (babel:octets-to-string (read-byte-vector function
                                            (read-integer function))
                          :encoding :utf-8))

(defgeneric binary-parse-from-type-tag (type-tag function)
  (:method ((tag (eql 1)) function)
    (read-string function))
  (:method ((tag (eql 2)) function)
    (read-byte-vector function (read-integer function)))
  (:method ((tag (eql 3)) function)
    (read-integer function))
  (:method ((tag (eql 4)) function)
    (let ((integer (read-integer function)))
      (if (zerop integer)
          (squirty-bottle "Don't use the negative-integer tag for zeroes")
          (- integer))))
  (:method ((tag (eql 5)) function)
    (assert (plusp *reader-depth*) () "too much list recursion")
    (let ((length (read-integer function)))
      (loop repeat length
            collect (binary-parse-from-function function))))
  (:method ((tag (eql 6)) function)
    (make-instance 'reference
                   :hash (read-string function)))
  (:method ((tag (eql 7)) function)
    (declare (ignore function))
    :true)
  (:method ((tag (eql 8)) function)
    (declare (ignore function))
    :false))

(defun binary-parse-from-function (function)
  (binary-parse-from-type-tag (funcall function) function))

(defun binary-parse (function-or-vector)
  (if (vectorp function-or-vector)
      (let ((position 0)
            (vector function-or-vector))
        (binary-parse-from-function
         (lambda ()
           (prog1 (aref vector position)
             (incf position)))))
      (binary-parse-from-function function-or-vector)))

(defun read-signatures (function)
  (loop repeat (read-integer function)
        collect (cons (make-instance 'reference
                                     :hash (read-byte-vector function 32))
                      (read-byte-vector function 64))))

(defun read-hash-table (function)
  (let ((size (read-integer function)))
    (assert (< size 5000))
    (let ((table (make-hash-table :test 'equal :size size)))
      (dotimes (n size)
        (setf (gethash (binary-parse-from-function function) table)
              (binary-parse-from-function function)))
      table)))

(defun binary-parse-block-from-function (function &key source name)
  (make-instance 'vague-object
                 :name name :source source
                 :signatures (read-signatures function)
                 :metadata (read-hash-table function)
                 :values   (read-hash-table function)
                 :computed-values (read-hash-table function)))

(defun binary-parse-block (function-or-vector &key source name)
  (if (vectorp function-or-vector)
      (let ((position 0)
            (vector function-or-vector))
        (binary-parse-block-from-function
         (lambda ()
           (prog1 (aref vector position)
             (incf position)))
         :source source :name name))
      (binary-parse-block-from-function function-or-vector
                                        :source source
                                        :name name)))
