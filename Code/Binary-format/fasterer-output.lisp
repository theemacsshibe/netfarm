(in-package :netfarm)

(defun make-output-vector (parts position)
  (let ((output-vector (make-array position
                                   :element-type '(unsigned-byte 8)))
        (start 0))
    (declare (optimize (speed 3) (safety 0))
             (fixnum start))
    (loop for part of-type (simple-array (unsigned-byte 8) (*))
            in (reverse parts)
          do (loop for output-position of-type fixnum from start
                   for input-position of-type fixnum below (length part)
                   for byte = (aref part input-position)
                   do (setf (aref output-vector output-position) byte))
             (incf start (length part)))
    output-vector))

(defmacro with-output-to-vector ((function-name) &body body)
  (alexandria:with-gensyms (parts position)
    `(let ((,parts '())
           (,position 0))
       (declare (fixnum ,position))
       (flet ((,function-name (vector)
                (declare ((simple-array (unsigned-byte 8) (*)) vector))
                (push vector ,parts)
                (incf ,position (length vector))))
         ,@body
         (make-output-vector ,parts ,position)))))
