(in-package :netfarm)

(declaim (inline write-type-tag write-integer)
         (optimize (speed 3)))
(defun write-type-tag (tag function)
  (let ((v (make-array 1 :element-type '(unsigned-byte 8))))
    (setf (aref v 0) tag)
    (funcall function v)
    t))

(defun write-integer (integer function)
  (let ((byte-count (ceiling (integer-length integer) 8)))
    (assert (< byte-count 256))
    (write-type-tag byte-count function)
    (loop with buffer = (make-array byte-count
                                    :element-type '(unsigned-byte 8))
          for byte-position from (1- byte-count) downto 0
          for step = integer then (ash step -8)
          do (setf (aref buffer byte-position)
                   (logand step #xFF))
          finally (funcall function buffer))))

(defgeneric binary-render-to-function (object function)
  (:method ((s string) function)
    (let ((bytes (babel:string-to-octets s :encoding :utf-8)))
      (write-type-tag 1 function)
      (write-integer (length bytes) function)
      (funcall function bytes)))
  (:method ((v vector) function)
    (write-type-tag 2 function)
    (write-integer (length v) function)
    (funcall function v))
  (:method ((i integer) function)
    (write-type-tag (if (minusp i) 4 3)
                    function)
    (write-integer (abs i) function))
  (:method ((l list) function)
    (write-type-tag 5 function)
    (write-integer (length l) function)
    (dolist (element l)
      (binary-render-to-function element function)))
  (:method ((r reference) function)
    (let ((bytes (babel:string-to-octets (reference-hash r)
                                         :encoding :utf-8)))
      (write-type-tag 6 function)
      (write-integer (length bytes) function)
      (funcall function bytes)))
  (:method ((o object) function)
    (let ((bytes (babel:string-to-octets (hash-object* o)
                                         :encoding :utf-8)))
      (write-type-tag 6 function)
      (write-integer (length bytes) function)
      (funcall function bytes)))
  (:method ((x (eql ':true)) function)
    (write-type-tag 7 function))
  (:method ((x (eql ':false)) function)
    (write-type-tag 8 function)))

(defmacro do-bound-sorted-slots ((netfarm-name value object slot-function)
                                 (length &body pre-body)
                                 &body body)
  (alexandria:with-gensyms (slot-alist bound-alist sorted-alist
                            slot pair class)
    (alexandria:once-only (object)
      `(let* ((,class (class-of ,object))
              (,slot-alist (string-hash-table->alist (,slot-function ,class)))
              (,bound-alist (loop for ,pair in ,slot-alist
                                  for (nil . ,slot) = ,pair
                                  when (closer-mop:slot-boundp-using-class ,class
                                                                           ,object
                                                                           ,slot)
                                    collect ,pair))
              (,length (length ,bound-alist))
              (,sorted-alist (sort ,bound-alist #'string< :key #'car)))
         ,@pre-body
         (dolist (,slot ,sorted-alist)
           (destructuring-bind (,netfarm-name . ,slot) ,slot
             (let ((,value (closer-mop:slot-value-using-class ,class ,object ,slot)))
               ,@body)))))))
       

(defun write-object-values (object function)
  (do-bound-sorted-slots (name value object
                               netfarm-class-slot-table)
      (l (write-integer l function))
    (binary-render-to-function name function)
    (binary-render-to-function value function)))

(defun write-object-computed-values (object function)
  (do-bound-sorted-slots (name computed-values object
                               netfarm-class-computed-slot-table)
      (l (write-integer l function))
    (binary-render-to-function name function)
    (let ((equivalent-list
            (loop for computed-value in computed-values
                  collect (list (computed-value-cause computed-value)
                                (computed-value-value computed-value)))))
      (binary-render-to-function equivalent-list function))))

(defun write-hash-table (hash-table function)
  (let ((keys (sort (alexandria:hash-table-keys hash-table) #'string<)))
    (write-integer (length keys) function)
    (dolist (key keys)
      (binary-render-to-function key function)
      (binary-render-to-function (gethash key hash-table) function))))

(defgeneric binary-render-object-to-function (object function
                                              emit-computed-values?
                                              emit-signatures?)
  (:method ((object object) function emit-computed-values? emit-signatures?)
    (when emit-signatures?
      (write-integer (length (object-signatures object)) function)
      (dolist (signature-pair (object-signatures object))
        (destructuring-bind (user . signature)
            signature-pair
          (funcall function (netfarm:hash-object user))
          (funcall function signature)))
      (setf (gethash "schema" (object-metadata object))
            (netfarm-class-name (class-of object))))
    (write-hash-table (object-metadata object) function)
    (write-object-values object function)
    (when emit-computed-values?
      (write-object-computed-values object function)))
  (:method ((vague-object vague-object) function emit-computed-values? emit-signatures?)
    (when emit-signatures?
      (write-integer (length (vague-object-signatures vague-object))
                     function)
      (dolist (signature-pair (vague-object-signatures vague-object))
        (destructuring-bind (user . signature)
            signature-pair
          (funcall function (netfarm:hash-object user))
          (funcall function signature))))
    (write-hash-table (vague-object-metadata vague-object) function)
    (write-hash-table (vague-object-values vague-object) function)
    (when emit-computed-values?
      (write-hash-table (vague-object-computed-values vague-object) function))))

(defun binary-render (object &optional function)
  (if (null function)
      (with-output-to-vector (f)
        (binary-render-to-function object #'f))
      (binary-render-to-function object function)))

(defun binary-render-object (object &key function
                                         (emit-computed-values t)
                                         (emit-signatures t))
  (if (null function)
      (with-output-to-vector (f)
        (binary-render-object-to-function object #'f
                                          emit-computed-values
                                          emit-signatures))
      (binary-render-object-to-function object function
                                        emit-computed-values
                                        emit-signatures)))
