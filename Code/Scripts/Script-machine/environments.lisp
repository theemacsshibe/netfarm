(in-package :netfarm-scripts)

;;; Global state manipulation
;;; 01: (get-proc n) -- proc
(define-opcode 1 get-proc ((:entry-points entry-points :environment environment))
    (n)
  (let ((description (aref entry-points n)))
    (make-procedure :entry-point (procedure-description-entry-point description)
                    :arguments (procedure-description-arguments description)
                    :environment environment)))

;;; 06: (get-proc* n) -- proc
;;; Doesn't capture the lexical environment. Technically, top-level procedures
;;; will never access any lexical environment other than their arguments, but they
;;; still consume space. get-proc* in a way is to get-proc as tail-call is to call.
(define-opcode 6 get-proc* ((:entry-points entry-points)) (n)
  (let ((description (aref entry-points n)))
    (procedure-description-static-procedure description)))

;;; 02: (get-value n) -- value
(define-opcode 2 get-value ((:variables variables)) (n)
  (aref variables n))
;;; 03: (set-value! n) new-value --
(define-opcode* 3 set-value! ((:variables variables) new-value) (n)
  (setf (aref variables n) new-value))

;;; Environments
;;; 04: (get-env variable frame) -- value
(define-opcode 4 get-env ((:environment environment)) (variable frame)
  (aref (nth frame environment) variable))
;;; 05: (set-env! variable frame) new-value --
(define-opcode* 5 set-env! ((:environment environment) new-value)
    (variable frame)
  (setf (aref (nth frame environment) variable) new-value))

;;; 0B: (byte x) -- x
(define-opcode 11 byte (()) (byte) byte)
