(in-package :netfarm-scripts)

(define-condition script-machine-signal () ()
  (:documentation "A signal created by the script machine when it wants to do something with the Netfarm universe.
The idea of this signal protocol is that some supervisor steps the script machine, and when it receives a signal,
it can either answer the signal or stash the machine's state if it cannot answer right away."))

(define-condition done (script-machine-signal) ())

(define-condition get-object (script-machine-signal)
  ((name :initarg :name :reader get-object-name))
  (:documentation "Get the object by NAME and push it to the stack."))

(define-condition overflow (error)
  ((operation :initarg :operation :reader overflow-operation)
   (operands  :initarg :operands  :reader overflow-operands)))

(define-condition no-capability (error)
  ((operation :initarg :operation :reader no-capability-operation)
   (required-capabilities :initarg :required-capabilities
                          :reader no-capability-capabilities)
   (cause :initarg :cause :reader no-capability-cause)))
