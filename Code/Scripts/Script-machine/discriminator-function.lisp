(in-package :netfarm-scripts)

;;; This file implements another type of dispatch for the bytecode interpreter,
;;; which is similar to what is described in "Fast generic dispatch for Common
;;; Lisp" by Robert Strandh <http://metamodular.com/SICL/generic-dispatch.pdf>

;;; This dispatch is expected to be faster on machines with slower memory than
;;; registers; as it replaces one table lookup with binary search that can be
;;; performed entirely in a register set. It also avoids a couple of function
;;; calls and memory accesses per cycle, which yields very impressive performance
;;; gains. 

(defvar *discriminator-function* nil)
(defun ensure-discriminator-function ()
  (when (null *discriminator-function*)
    (setf *discriminator-function* (make-discriminator-function))))

(defmacro with-interpreter-parts-renamed ((&rest parts) &body body)
  `(symbol-macrolet ,(loop for (name variable) on parts by #'cddr
                           for new-name = (intern (string name) :netfarm-scripts)
                           unless (eql variable new-name)
                             collect `(,variable ,new-name))
     ,@body))

(defun make-discriminator-parts (opcodes)
  (cond
    ((null opcodes)
     (error "no opcodes to dispatch on?"))
    ((null (rest opcodes))
     (destructuring-bind (opcode) opcodes
       (if (null (aref *opcode-code* opcode))
           `(go lose)
           (destructuring-bind ((parts inputs bytes) declarations body)
               (aref *opcode-code* opcode)
             `(with-interpreter-parts-renamed (data-stack %%data-stack
                                               program-counter %%program-counter
                                               program %%program
                                               . ,parts)
                (let (,@(loop for input in (reverse inputs)
                              collect `(,input (pop* %%data-stack)))
                      ,@(loop for byte in bytes
                              for position from 0
                              collect `(,byte (aref %%program (+ ,position %%program-counter)))))
                  ,@declarations
                  (incf %%program-counter ,(length bytes))
                  ,body
                  (go out)))))))
    ((every (lambda (opcode)
              (null (aref *opcode-code* opcode)))
            opcodes)
     '(go lose))
    (t
     (let* ((middle (floor (length opcodes) 2))
            (middle-element (nth middle opcodes)))
       `(if (< opcode ,middle-element)
            ,(make-discriminator-parts (subseq opcodes 0 middle))
            ,(make-discriminator-parts (subseq opcodes middle)))))))

(defun make-discriminator-code ()
  (labels ((in-bounds? (opcode)
             (<= 0 opcode 255))
           (exists? (opcode)
             (and (in-bounds? opcode)
                  (not (null (aref *opcode-code* opcode)))))
           (needed? (opcode)
             (or (exists? opcode) (exists? (1+ opcode)) (exists? (1- opcode)))))
    (make-discriminator-parts
     (loop for opcode below 256
           when (needed? opcode)
             collect opcode))))

(defmacro let-interpreter-parts ((&rest names) &body body)
  `(let ,(loop for name in names
               collect `(,name (,(alexandria:format-symbol :netfarm-scripts
                                                           "INTERPRETER-~a"
                                                           name)
                                interpreter)))
     ,@body))

(defmacro put-back-interpreter-parts (&rest names)
  `(setf ,@(loop for name in names
                 appending `((,(alexandria:format-symbol :netfarm-scripts
                                                         "INTERPRETER-~a"
                                                         name)
                              interpreter)
                             ,name))))

(defun make-discriminator-function ()
  (compile nil
           `(lambda (interpreter cons-limit cycle-limit)
              (declare (optimize (speed 3) (safety 1)
                                 (debug 0))
                       (interpreter interpreter)
                       ((or null fixnum) cons-limit cycle-limit)
                       #+sbcl (sb-ext:muffle-conditions sb-ext:compiler-note))
              (let-interpreter-parts (program-counter variables program
                                      call-stack data-stack
                                      environment
                                      entry-points side-effects capabilities
                                      instruction-count cons-count)
                (declare (reasonably-sized-natural program-counter
                                                   instruction-count
                                                   cons-count)
                         (list environment side-effects capabilities
                               call-stack data-stack)
                         ((simple-array (unsigned-byte 8) (*)) program)
                         ((simple-array t (*)) variables)
                         ((simple-array procedure-description (*)) entry-points))
                (unwind-protect
                     (loop
                       (let ((opcode (aref program program-counter)))
                         (declare ((unsigned-byte 8) opcode))
                         (incf instruction-count)
                         (incf program-counter)
                         (tagbody
                            ,(make-discriminator-code)
                          lose
                            (error "illegal opcode ~d on ~s"
                                   opcode interpreter)
                          out
                            (when (and (not (null cons-limit))
                                  (> cons-count cons-limit))
                              (error "exceeded cons limit"))
                            (when (and (not (null cycle-limit))
                                       (> instruction-count cycle-limit))
                              (error "exceeded cycle limit")))))
                  (put-back-interpreter-parts program-counter
                                              call-stack data-stack
                                              environment
                                              side-effects
                                              instruction-count cons-count))))))
