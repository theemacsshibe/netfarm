(in-package :netfarm-scripts)

(defclass script ()
  ((entry-points :initarg :entry-points :reader script-entry-points)
   (program :initarg :program :reader script-program)
   (variables :initarg :variables :reader script-variables))
  (:metaclass netfarm::inbuilt-netfarm-class)
  (:schema-name "inbuilt@script"))

(deftype reasonably-sized-natural ()
  "Well, I hope this is a subset of the implementation's FIXNUM type. But still." 
  '(unsigned-byte 30))
(declaim (inline make-procedure))
(defstruct procedure
  (entry-point 0 :type reasonably-sized-natural)
  (arguments 0 :type reasonably-sized-natural)
  (environment '() :type list))
(defstruct procedure-description
  (entry-point 0 :type reasonably-sized-natural)
  (arguments 0 :type reasonably-sized-natural)
  (static-procedure (make-procedure :entry-point 0
                                    :arguments 0
                                    :environment '())
                    :type procedure))
(defstruct interpreter
  ;; Program state
  (script nil :read-only t)
  (program-counter 0 :type reasonably-sized-natural)
  (variables #() :type (simple-array t (*)) :read-only t)
  (program (make-array 0 :element-type '(unsigned-byte 8)
                         :initial-element 0)
           :type (simple-array (unsigned-byte 8) (*)))
  (call-stack '() :type list)
  (data-stack '() :type list)
  ;; Environment
  (environment '() :type list)
  (entry-points (make-array 0 :element-type 'procedure-description)
                :type (simple-array procedure-description (*)))
  (side-effects '() :type list)
  (capabilities '() :read-only t)
  ;; Watchdog limits
  (instruction-count 0 :type fixnum)
  (cons-count 0 :type fixnum))

(declaim (inline interpreter-read-byte interpreter-read-byte*))
(defun interpreter-read-byte (interpreter)
  (declare (interpreter interpreter))
  (prog1 (aref (interpreter-program interpreter)
               (interpreter-program-counter interpreter))
    (incf (interpreter-program-counter interpreter))))

(defun interpreter-read-byte* (interpreter &optional (offset 0))
  (declare (interpreter interpreter) (fixnum offset))
  (aref (interpreter-program interpreter)
        (+ offset (interpreter-program-counter interpreter))))

(declaim (inline step-interpreter))
(defun step-interpreter (interpreter operators)
  (declare (interpreter interpreter))
  (let* ((opcode (interpreter-read-byte interpreter))
         (function (aref operators opcode)))
    (incf (interpreter-instruction-count interpreter))
    (funcall function interpreter opcode)))

(defmacro pop* (place)
  `(if (null ,place)
       (error "stack underflow")
       (pop ,place)))

(defun make-byte-array (array)
  (alexandria:copy-array array
			 :element-type '(unsigned-byte 8)
			 :fill-pointer nil
			 :adjustable nil))

(defun entry-point-vector (entry-point-list)
  (let ((vector (make-array (length entry-point-list)
                            :element-type 'procedure-description
                            :initial-element (make-procedure-description))))
    (loop for entry in entry-point-list
          for n from 0
          do (destructuring-bind (entry-point arguments) entry
               (setf (aref vector n)
                     (make-procedure-description
                      :entry-point entry-point
                      :arguments arguments
                      :static-procedure (make-procedure
                                         :entry-point entry-point
                                         :arguments arguments
                                         :environment '())))))
    vector))

(defun setup-interpreter (script arguments &optional capabilities)
  (declare (script script)
           (list arguments))
  (let* ((interpreter
           (make-interpreter :script script
                             :program (make-byte-array (script-program script))
                             :variables (coerce (script-variables script) 'vector)
                             :data-stack (reverse arguments)
                             :entry-points (entry-point-vector (script-entry-points script))
                             :capabilities capabilities))
         (description (aref (interpreter-entry-points interpreter) 0)))
    (call-function interpreter
                   (make-procedure :entry-point (procedure-description-entry-point description)
                                   :arguments (procedure-description-arguments description)
                                   :environment '())
                   (length arguments))
    interpreter))

(defun run-interpreter (interpreter &key cons-limit cycle-limit print-cycles step)
  (declare (interpreter interpreter)
           ((or null fixnum) cons-limit cycle-limit)
           (boolean print-cycles step))
  (ensure-discriminator-function)
  (let (#+netfarm-profiling
        (opcode-count-table (make-hash-table))
	(discriminator-function *discriminator-function*)
        (operators *operators*))
    (declare ((function (interpreter t t) t) discriminator-function))
    (handler-case
        (if (and (not print-cycles) (not step))
            (funcall discriminator-function interpreter cons-limit cycle-limit)
            (loop
              #+netfarm-profiling
               (incf (gethash (first (aref *opcode-data* (interpreter-read-byte* interpreter)))
                              opcode-count-table 0))
               (when print-cycles
                 (let ((*print-circle* t))
                   (print
                    `(:pc ,(interpreter-program-counter interpreter)
                      :data ,(interpreter-data-stack interpreter)
                      :env ,(interpreter-environment interpreter))))
                 (let ((opcode-info (aref *opcode-data* (interpreter-read-byte* interpreter))))
                   (unless (null opcode-info)
                     (format t "(~a" (first opcode-info))
                     (loop for byte-name in (third opcode-info)
                           for offset from 1
                           for value = (interpreter-read-byte* interpreter offset)
                           do (format t " :~a ~d" byte-name value))
                     (format t ")~%"))))
               (step-interpreter interpreter operators)
               (when (and (not (null cons-limit))
                          (> (interpreter-cons-count interpreter) cons-limit))
                 (error "exceeded cons limit"))
               (when (and (not (null cycle-limit))
                          (> (interpreter-instruction-count interpreter) cycle-limit))
                 (error "exceeded cycle limit"))
               (when step
                 (read-line))))
      (done ()
        (return-from run-interpreter
          (values (interpreter-data-stack interpreter)
                  interpreter
                  #+netfarm-profiling opcode-count-table))))))

(defun add-object-to-interpreter (interpreter object)
  (push object (interpreter-data-stack interpreter))
  (values))
