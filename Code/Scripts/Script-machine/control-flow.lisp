 (in-package :netfarm-scripts)

(declaim (inline call-function make-frame))
(defun call-function (interpreter function argument-count &key (save-current t))
  (check-type function procedure)
  (assert (= argument-count (procedure-arguments function)) ()
          "wanted ~d argument~:p but got ~d"
          (procedure-arguments function) argument-count)
  (when save-current
    (push (vector (interpreter-environment interpreter)
                  (interpreter-program-counter interpreter))
          (interpreter-call-stack interpreter)))
  (setf (interpreter-environment interpreter)
        (cons (make-frame interpreter argument-count)
              (procedure-environment function))
        (interpreter-program-counter interpreter)
        (procedure-entry-point function)))

(defun make-frame (interpreter frame-size)
  (let ((frame (make-array frame-size)))
    (loop for n from (1- frame-size) downto 0
          when (null (interpreter-data-stack interpreter))
            do (error "out of data when creating frame")
          do (setf (aref frame n)
                   (pop (interpreter-data-stack interpreter))))
    frame))

;;; Control flow
;;; 08: (return) --
(define-opcode* 8 return ((:call-stack call-stack :environment environment
                           :program-counter program-counter))
                         ()
  (when (null call-stack)
    (error "(return) with nowhere to return to"))
  (let ((last-frame (pop call-stack)))
    (locally (declare (optimize (safety 0) (speed 3))))
    (setf environment (svref last-frame 0)
          program-counter (svref last-frame 1)))
  (when (null call-stack)
    (signal 'done)))

;;; 09: (call n) arg-1 ... arg-n function --
(define-opcode* 9 call ((:cons-count cons-count :data-stack data-stack
                         :call-stack call-stack
                         :program-counter program-counter
                         :environment environment)
                        function)
    (argument-count)
  (incf cons-count argument-count)
  (check-type function procedure)
  (assert (= argument-count (procedure-arguments function)) ()
          "wanted ~d argument~:p but got ~d"
          (procedure-arguments function) argument-count)
  (push (vector environment program-counter) call-stack)
  (let ((new-frame (make-array argument-count)))
    (loop for n from (1- argument-count) downto 0
          when (null data-stack)
            do (error "out of data when creating frame")
          do (setf (aref new-frame n)
                   (pop data-stack)))
    (setf environment
          (cons new-frame environment)
          program-counter (procedure-entry-point function))))
  
;;; 0A: (tail-call n) arg-1 ... arg-n function --
;;; Similar to CALL, but doesn't back up the last state on the stack. This is
;;; therefore only good for tail-calls. It's really good at those though.
(define-opcode* 10 tail-call ((:cons-count cons-count :data-stack data-stack
                               :call-stack call-stack
                               :program-counter program-counter
                               :environment environment)
                              function)
    (argument-count)
  (pop environment)
  (check-type function procedure)
  (assert (= argument-count (procedure-arguments function)) ()
          "wanted ~d argument~:p but got ~d"
          (procedure-arguments function) argument-count)
  (let ((new-frame (make-array argument-count)))
    (loop for n from (1- argument-count) downto 0
          when (null data-stack)
            do (error "out of data when creating frame")
          do (setf (aref new-frame n)
                   (pop data-stack)))
    (setf environment
          (cons new-frame environment)
          program-counter (procedure-entry-point function))))

(declaim (inline u8-pair->u16))
(defun u8-pair->u16 (high low)
  (logior (ash high 8)
          low))

;;; 10: (cond-jump then-high then-low else-high else-low) test-value --
;;; Let then = then-high * 256 + then-low,
;;;     else = else-high * 256 + else-low
;;; Jump forwards `then` bytes if test-value is true, else jump forwards `else` bytes.
(define-opcode* 16 jump-cond ((:program-counter program-counter) test-value)
    (then-high then-low else-high else-low)
  (let ((then (u8-pair->u16 then-high then-low))
        (else (u8-pair->u16 else-high else-low)))
    (if (eql test-value :false)
        (incf program-counter else)
        (incf program-counter then))))

(define-opcode* 17 jump ((:program-counter program-counter))
    (high low)
  (incf program-counter (u8-pair->u16 high low)))

;;; 18: (error) message cause --
(define-opcode* 24 error (() message cause) ()
  (error 'script-machine-error :message message :cause cause))
