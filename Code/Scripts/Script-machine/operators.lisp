(in-package :netfarm-scripts)

;;; List processing

(defun kons (first rest)
  "CONS that only makes proper lists"
  (check-type rest list)
  (cons first rest))

(defmacro boolean-wrap (function-name argument-count &key (type t))
  (let ((arguments (loop repeat argument-count collect (gensym "ARGUMENT"))))
    `(lambda ,arguments
       (declare (,type . ,arguments))
       (if (,function-name . ,arguments)
           :true :false))))

(define-opcode 64 cons ((:cons-count cons-count) first rest) ()
  (incf cons-count)
  (kons first rest))
(define-opcode 65 car (() cons) ()
  (check-type cons cons)
  (car cons))
(define-opcode 66 cdr (() cons) ()
  (check-type cons cons)
  (cdr cons))
(define-function-opcode 67 (boolean-wrap null 1) 1 :opcode-name null)
(define-function-opcode 68 consp 1)
(define-opcode 69 append ((:cons-count cons-count) list1 list2) ()
  (incf cons-count
        (+ (length list1) (length list2)))
  (append list1 list2))
(define-function-opcode 70 (boolean-wrap eql 2) 2 :opcode-name eql)

;;; Generic equal that's slightly less picky than EQUAL because it recurses trees.
(defgeneric netfarm-equal (value1 value2)
  (:method (v1 v2) (eql v1 v2))
  (:method ((v1 string) (v2 string)) (string= v1 v2))
  (:method ((v1 list) (v2 list))
    (= (length v1) (length v2))
    (every #'netfarm-equal v1 v2))
  (:method ((v1 vector) (v2 vector))
    (= (length v1) (length v2))
    (every #'= v1 v2)))
(define-function-opcode 71 netfarm-equal 2 :opcode-name equal)
(define-function-opcode 72 (boolean-wrap string= 2) 2 :opcode-name string=)


(define-opcode 73 list ((:cons-count cons-count :data-stack data-stack))
    (count)
  (incf cons-count count)
  (reverse
   (loop repeat count
         collect (pop* data-stack))))

;;; Number processing

;; log2(a + b) > 256 if 1+ log2(max(a,b)) > 256
(define-opcode 96 + (() a b) ()
  (when (> (integer-length (max a b)) 255)
    (error 'overflow :operands (list a b) :operation '+))
  (+ a b))
(define-opcode 97 - (() a b) ()
  (when (> (integer-length (max a b)) 255)
    (error 'overflow :operands (list a b) :operation '-))
  (- a b))
;; log2(ab) > 256 if log2(a) + log2(b) > 256
(define-opcode 98 * (() a b) ()
  (when (> (+ (integer-length a) (integer-length b)) 255)
    (error 'overflow :operands (list a b) :operation '*))
  (* a b))
(define-function-opcode 99 floor 2 :opcode-name /)
(define-function-opcode 100 abs 1)

(define-function-opcode 112 (boolean-wrap = 2)  2 :opcode-name =)
(define-function-opcode 113 (boolean-wrap < 2)  2 :opcode-name <)
(define-function-opcode 114 (boolean-wrap > 2)  2 :opcode-name >)
(define-function-opcode 115 (boolean-wrap /= 2) 2 :opcode-name /=)
(define-function-opcode 116 (boolean-wrap <= 2) 2 :opcode-name <=)
(define-function-opcode 117 (boolean-wrap >= 2) 2 :opcode-name >=)
