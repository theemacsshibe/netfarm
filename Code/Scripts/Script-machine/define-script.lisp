(in-package :netfarm-scripts)

(defmacro define-script (variable-name (&rest variables) &body body)
  `(defparameter ,variable-name
     (make-instance (find-class 'script)
                    :entry-points (get-entry-points ',body)
                    :program (assemble ',body)
                    :variables ',variables)))
