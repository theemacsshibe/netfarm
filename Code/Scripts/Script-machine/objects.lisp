(in-package :netfarm-scripts)

;;; Manipulating Netfarm objects

;;; (get-object) name -- object
(define-opcode 128 get-object (() name) ()
  (check-type name string)
  (error 'get-object :name name))

(define-opcode 129 schema (() object) ()
  (if (typep object 'netfarm:object)
      (class->schema (class-of object))
      (error "~s does not have a schema" object)))

;;; (add-computed-value) name value --
(define-opcode* 130 add-computed-value ((:capabilities capabilities
                                         :side-effects side-effects)
                                        name value)
    ()
  (check-type name string)
  (if (member :add-computed-value capabilities)
      (push (list 'add-computed-value :name name
                                      :value value)
            side-effects)
      (error 'no-capability
             :operation 'add-computed-value
             :required-capabilities '(:add-computed-value)
             :cause value)))

(define-function-opcode 131 object-value 2)

(define-opcode 132 object-computed-values ((:capabilities capabilities)
                                           object slot-name)
    ()
  (if (member :computed-values capabilities)
      (netfarm:object-computed-values object slot-name)
      (error 'no-capability
             :operation 'object-computed-values
             :required-capabilities '(:required-capabilities)
             :cause object)))

(define-function-opcode 133 (boolean-wrap object-boundp 2) 2
  :opcode-name object-boundp)

(define-opcode 134 object-signatures ((:cons-count cons-count)
                                      object)
    ()
  (let ((signatures (object-signatures object)))
    (incf cons-count (length signatures))
    (mapcar #'car signatures)))

(define-opcode* 135 send ((:capabilities capabilities
                           :side-effects side-effects)
                          target message)
    ()
  (if (member :send capabilities)
      (push (list 'send :target target
                        :message message)
            side-effects)
      (error 'no-capability
             :operation 'send
             :required-capabilities '(:send)
             :cause message)))
