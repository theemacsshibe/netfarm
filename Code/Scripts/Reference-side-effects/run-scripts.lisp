(in-package :netfarm-scripts)

(defvar *initialization-script-instruction-limit* 100000)
(defvar *message-script-instruction-limit* 10000)

(defgeneric filter-effect-against (filter type &key &allow-other-keys)
  (:documentation "Is this message provided allowed by the filter and by Netfarm messaging rules?") 
  (:method (filter (type (eql 'send)) &key target message)
    (declare (ignore message))
    ;; Targets also have to be objects and have to have message scripts.
    (and (typep target 'object)
         (not (null (netfarm-class-message-script
                     (class-of target))))
         (funcall filter target))))

(defun apply-recipient-filter (effects filter)
  "Keep all effects allowed by the filter."
  (remove-if-not (lambda (effect)
                   (apply #'filter-effect-against filter effect))
                 effects))

(defun accumulate-messages (object &key (recipient-filter (constantly t)))
  (let ((scripts (netfarm-class-scripts (class-of object)))
        (accumulated-messages '()))
    (dolist (script scripts)
      (multiple-value-bind (stack interpreter)
          (run-interpreter (setup-interpreter script
                                              (list object)
                                              '(:send)))
        (declare (ignore stack))
        (setf accumulated-messages
              (append (apply-recipient-filter (interpreter-side-effects interpreter)
                                              recipient-filter)
                      accumulated-messages))))
    accumulated-messages))

(defgeneric make-message-interpreter (cause type &key &allow-other-keys)
  (:method (cause (type (eql 'send)) &key target message)
    (values target
            (setup-interpreter (netfarm-class-message-script (class-of target))
                               (list target cause message)
                               '(:add-computed-value)))))

(defun accumulate-side-effects (cause messages)
  "Create a hash table of objects and the side effects their scripts create."
  (let ((side-effect-table (make-hash-table)))
    (dolist (message messages)
      (multiple-value-bind (target interpreter)
          (apply #'make-message-interpreter cause message)
        (multiple-value-bind (stack interpreter)
            (run-interpreter interpreter
                             :cons-limit *message-script-instruction-limit*
                             :cycle-limit *message-script-instruction-limit*)
          (declare (ignore stack))
          (setf (gethash target side-effect-table)
                (append (interpreter-side-effects interpreter)
                        (gethash target side-effect-table))))))
    side-effect-table))

(defun run-script-machines (object &key (apply-side-effects t)
                                        (recipient-filter (constantly t)))
  (check-type object object)
  (let* ((accumulated-messages (accumulate-messages object
                                                    :recipient-filter recipient-filter))
         (side-effects (accumulate-side-effects object accumulated-messages)))
    (when apply-side-effects
      (maphash (lambda (target effects)
                 (dolist (effect effects)
                   (apply #'apply-side-effect object target effect)))
               side-effects))
    (values accumulated-messages side-effects)))
    
