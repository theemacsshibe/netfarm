(in-package :netfarm-scripts)

(defgeneric apply-side-effect (cause target type &key &allow-other-keys)
  (:method (cause target (type (eql 'add-computed-value)) &key name value)
    (push (make-instance 'computed-value :cause cause
                                         :value value)
          (object-computed-values target name))))
