(defpackage :netfarm-scripts
  (:use :cl :netfarm)
  (:export #:assemble #:define-script #:script #:opcode-information
           #:interpreter #:interpreter-side-effects #:interpreter-cause
           #:interpreter-instruction-count #:interpreter-cons-count
           #:run-interpreter #:setup-interpreter
           #:add-computed-value #:send
           #:add-object-to-interpreter #:get-object #:get-object-name
           #:run-script-machines))
