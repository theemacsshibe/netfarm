(defpackage :netfarm
  (:use :cl :ironclad :s-base64 :flexi-streams :split-sequence)
  (:export
   ;; formats
   #:binary-render #:binary-render-object
   #:parse-block #:render-object
   #:parse #:render
   #:binary-parse #:binary-parse-block
   #:apply-class
   #:*reader-depth*
   #:base64->bytes #:bytes->base64
   ;; objects
   #:netfarm-class #:netfarm-class-scripts #:netfarm-class-message-script
   #:netfarm-class-presentation-script
   #:netfarm-class-name
   #:object-value #:set-object-value #:object-makunbound #:object-boundp
   #:map-objects #:do-objects
   #:compute-dependencies #:deep-copy-object
   #:find-netfarm-class
   #:find-netfarm-slot #:find-netfarm-computed-slot
   #:map-slots #:map-computed-slots
   #:class->schema
   ;; types
   #:object #:vague-object #:reference
   #:bad-netfarm-type-specifier
   #:netfarm-type->cl-type
   #:netfarm-typep #:check-netfarm-type
   ;; accessors
   #:vague-object-signatures #:vague-object-name
   #:vague-object-metadata #:vague-object-computed-values
   #:vague-object-source #:vague-object-values
   #:vague-object-schema-name
   #:map-references
   #:object-signatures
   #:object-metadata #:object-computed-values
   #:object-source
   #:object-changed-p #:object-set-checkpoint
   #:slot-name #:slot-reader-type
   #:reference-hash
   ;; schema
   #:schema #:schema-slots #:schema-scripts
   #:schema-message-scripts #:schema-presentation-script
   #:schema->class
   ;; computed values
   #:computed-value #:computed-value-cause #:computed-value-value
   #:add-computed-value
   ;; inbuilt classes
   #:user #:schema
   #:user-map #:user-map-antitriples #:user-map-triples #:user-map-delegates
   ;; keys
   #:generate-keys #:make-keys
   #:keys-from-public #:shared-key
   #:keys-signature-public #:keys-signature-private
   #:keys-exchange-public #:keys-exchange-private
   #:symmetric-cipher
   #:symmetric-encrypt #:symmetric-decrypt
   #:keys
   ;; hashes
   #:hash-text #:sign-text
   #:hash-object #:hash-object*
   #:sign-object #:add-signature
   #:verify-text #:verify-object-hash #:verify-object-signatures
   #:readable-verifier
   #:object->keys #:keys->object
   #:with-hash-cache #:with-lexical-hash-cache #:with-restored-lexical-hash-cache
   ;; codecs
   #:define-codec #:make-codec
   #:codec-encoder #:codec-decoder
   #:*codecs*))
