(in-package :netfarm)

(define-condition bad-netfarm-type-specifier (error)
  ((specifier :initarg :specifier
              :reader bad-netfarm-type-specifier-specifier))
  (:report (lambda (c s)
             (format s "~s is not a valid Netfarm type specifier."
                     (bad-netfarm-type-specifier-specifier c)))))
(define-condition netfarm-type-error (type-error)
  ((type-string :initarg :type-string
                :reader netfarm-type-error-string)
   (place :initarg :place
          :reader netfarm-type-error-place))
  (:report (lambda (c s)
             (format s "~s is ~s, which is not ~:[of type ~s~;~:*~a~]."
                     (netfarm-type-error-place c)
                     (type-error-datum c)
                     (netfarm-type-error-string c)
                     (type-error-expected-type c)))))

(defun bad-netfarm-type-specifier (specifier)
  (error 'bad-netfarm-type-specifier :specifier specifier))

(defun netfarm-type->cl-type (type-specifier)
  "Convert a Netfarm type specifier to an approximately equal Common Lisp type specifier.
To test if an object is of a Netfarm type, you probably should use NETFARM-TYPEP as it is more precise in corner cases like (list <t>)."
  (typecase type-specifier
    (schema (schema->class type-specifier))
    (netfarm-class type-specifier)
    (string (alexandria:switch (type-specifier :test #'string=)
              ("boolean" '(member :true :false))
              ("list" 'list)
              ("integer" '(signed-byte 256))
              ("octet-array" 'vector)
              ("string" 'string)
              ("top" 'netfarm-serializable-type)
              (otherwise (bad-netfarm-type-specifier type-specifier))))
    (list (alexandria:switch ((first type-specifier) :test #'string=)
            ;; We can't express the type "a list of elements of type <T>" in the
            ;; Common Lisp type system.
            ("list"
             (unless (= (length type-specifier) 2)
               (bad-netfarm-type-specifier type-specifier))
             'list)
            ("or" (cons 'or (mapcar #'netfarm-type->cl-type type-specifier)))
            (otherwise (bad-netfarm-type-specifier type-specifier))))
    (t (bad-netfarm-type-specifier type-specifier))))
              
(defun netfarm-typep (object type-specifier)
  "Test if an object is of a Netfarm type."
  (typecase type-specifier
    (schema (eq (class-of object)
                (schema->class type-specifier)))
    (netfarm-class (eq (class-of object) type-specifier))
    (string (alexandria:switch (type-specifier :test #'string=)
              ("boolean" (or (eql object :true)
                             (eql object :false)))
              ("list" (listp object))
              ("integer" (typep object '(signed-byte 256)))
              ("string" (stringp object))
              ("octet-array" (and (vectorp object)
                                  (every (lambda (x)
                                           (typep x '(unsigned-byte 8)))
                                         object)))
              ("top" (typep object 'netfarm-serializable-type))
              (otherwise (bad-netfarm-type-specifier type-specifier))))
    (list (alexandria:switch ((first type-specifier) :test #'string=)
            ("list"
             (unless (= (length type-specifier) 2)
               (bad-netfarm-type-specifier type-specifier))
             (every (lambda (x)
                      (netfarm-typep x (second type-specifier)))
                    object))
            ("or"
             (some (lambda (subtype) (netfarm-typep object subtype))
                   (rest type-specifier)))
            (otherwise (bad-netfarm-type-specifier type-specifier))))
    (t (bad-netfarm-type-specifier type-specifier))))

(defmacro check-netfarm-type (place type &optional type-string)
  "Check that the place is of the type. The optional TYPE-STRING can be used to provide an alternate name for the type that is provided to the user."
  (labels ((rewrite-symbol (thing)
             (typecase thing
               (symbol (string-downcase thing))
               (list (mapcar #'rewrite-symbol thing))
               (t thing))))
    (let ((type (rewrite-symbol type)))
      `(loop until (netfarm-typep ,place ',type)
             do (setf ,place
                      (restart-case (error 'netfarm-type-error
                                           :place ',place
                                           :expected-type ',type
                                           :datum ,place
                                           :type-string ',type-string)
                        (store-value (value)
                          :report (lambda (s)
                                    (format s "Store a new value in ~s" ',place))
                          :interactive (lambda (&rest arguments)
                                         (format *query-io* "Enter a new value for ~s: "
                                                 ',place)
                                         (finish-output *query-io*)
                                         (list (eval (read *query-io*))))
                          value)))))))
                                       
       
