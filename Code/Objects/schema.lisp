(in-package :netfarm)

(defun apply-class (vague-object class)
  "Apply the class CLASS to a VAGUE-OBJECT, matching slot names and types with 
keys in the vague-object's values table."
  (let ((new-object (make-instance class
                                   :source (vague-object-source vague-object)
                                   :metadata (vague-object-metadata vague-object)
                                   :signatures (vague-object-signatures vague-object)))
        (data (vague-object-values vague-object)))
    (loop for key being the hash-keys in data
          for value being the hash-values in data
          for slot-definition = (find-netfarm-slot class key)
          do (assert (not (closer-mop:slot-boundp-using-class class new-object
                                                              slot-definition)))
             (setf (closer-mop:slot-value-using-class class new-object
                                                      slot-definition)
                   value))
    (maphash (lambda (name values)
	       (setf (object-computed-values new-object name)
                     (loop for (cause value) in values
                           collect (make-instance 'computed-value
                                                  :cause cause
                                                  :value value))))
	     (vague-object-computed-values vague-object))
    new-object))

(define-condition netfarm-class-not-found (error)
  ((class-name :initarg :class-name
               :reader netfarm-class-not-found-class-name))
  (:report
   (lambda (c s)
     (format s "There is no class named ~s."
             (netfarm-class-not-found-class-name c)))))

(defun find-netfarm-class (name &optional (error? t))
  "Find a Netfarm class with the provided name. If no such class exists and ERROR? is true, a NETFARM-CLASS-NOT-FOUND error will be signalled. If no such class exists and ERROR? is false, NIL will be returned."
  (or (gethash name *classes*)
      (gethash name *inbuilt-classes*)
      (if error?
          (error 'netfarm-class-not-found :class-name name)
          nil)))

;;; Convert schemas to classes and back.

(defun getf/string (name plist &optional default)
  (loop for (key value) on plist by #'cddr
        when (string= name key)
          return (values value t))
  (values default nil))

(defun schema-direct-slots (schema slot-names)
  (append
   (loop for (slot-name . properties) in (schema-slots schema)
         collect `(:name ,(or (cdr (assoc slot-name slot-names :test #'string=))
                              (make-symbol slot-name))
                   :documentation ,(getf/string "documentation" properties)))
   (loop for (slot-name . properties) in (schema-computed-slots schema)
         collect `(:name ,(or (cdr (assoc slot-name slot-names :test #'string=))
                              (make-symbol slot-name))
                   :computed t
                   :documentation ,(getf/string "documentation" properties)))))

(defmacro with-cache ((key table &key (write-back t)) &body body)
  (alexandria:once-only (key table)
    (alexandria:with-gensyms (value present?)
      `(multiple-value-bind (,value ,present?)
           (gethash ,key ,table)
         (if ,present?
             ,value
             ,(if write-back
                  `(setf (gethash ,key ,table)
                         (progn ,@body))
                  `(progn ,@body)))))))

(defun schema->class (schema &key slot-names)
  "Convert a schema into a NETFARM-CLASS. If a class already exists that corresponds to the schema, it is returned. Otherwise, a new schema is returned. If the association list SLOT-NAMES is provided, each slot name will be looked up in the association list, and the values will be used as slot names instead of creating uninterned symbols."
  (with-cache ((hash-object* schema) *classes* :write-back nil)
    (make-instance 'netfarm-class
     :name (make-symbol (hash-object* schema))
     :documentation (schema-documentation schema)
     :direct-slots (schema-direct-slots schema slot-names)
     :scripts (schema-scripts schema)
     :presentation-script (schema-presentation-script schema)
     :message-script (schema-message-script schema))))

(defun slot-definition->netfarm-slot (name slot-definition)
  (declare (ignore slot-definition))
  `(,name))

(defun slot-definition-table->slot-definitions (slot-definition-table)
  (loop for (name . slot-definition)
          in (string-hash-table->alist slot-definition-table)                               
        collect (slot-definition->netfarm-slot name slot-definition)))

(defun make-class-schema (class &rest initargs)
  (apply #'make-instance 'schema
         :computed-slots (slot-definition-table->slot-definitions
                          (netfarm-class-computed-slot-table class))
         :documentation (documentation class 'type)
         :message-script (netfarm-class-message-script class)
         :presentation-script (netfarm-class-presentation-script class)
         :scripts (netfarm-class-scripts class)
         :slots (slot-definition-table->slot-definitions
                 (netfarm-class-slot-table class))
         initargs))

(defgeneric %class->schema (class)
  (:method ((class inbuilt-netfarm-class))
    (make-class-schema class :inbuilt-name (netfarm-class-name class)))
  (:method ((class netfarm-class))
    (make-class-schema class)))

(defun class->schema (class)
  "Convert a NETFARM-CLASS into a schema. If a schema for this class already exists, it is returned."
  (with-cache (class *schemas*) (%class->schema class)))
