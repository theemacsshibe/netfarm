(in-package :netfarm)

(deftype netfarm-serializable-type ()
  '(or reference
       string
       (array (unsigned-byte 8) (*))
       list
       integer
       (member :true :false)))

(define-tuple reference ()
  ((hash :read-only t))
  "A reference to an object.")

(macrolet ((mht () '(make-hash-table :test 'equal)))
  (define-tuple vague-object ()
    ((source :initform nil)
     (signatures :initform nil)
     (metadata :initform (mht))
     (values :initform (mht))
     (name :initform nil)
     (computed-values :initform (mht)))
    "A parsed, but not schema-passed, object."))

(defmacro with-slot-definition ((name class object netfarm-name &key (find-slot 'find-netfarm-slot)) &body body)
  (alexandria:once-only (object netfarm-name)
    `(let* ((,class (class-of ,object))
            (,name (,find-slot ,class ,netfarm-name)))
       (assert ,name () "Slot ~s is not present in the object ~s" ,netfarm-name ,object)
       ,@body)))

;;; s/slot/object/-modified CLOS accessors & functions that work with Netfarm
;;; slot names rather than symbols.
(declaim (inline object-value (setf object-value)
                 object-boundp object-makunbound
                 object-computed-values (setf object-computed-values)))
(defun object-value (object slot-name)
  "Get the value of the slot named SLOT-NAME in the object OBJECT."
  (with-slot-definition (slot class object slot-name)
    (closer-mop:slot-value-using-class class object slot)))

(defun (setf object-value) (new-value object slot-name)
  "Set the value of the slot named SLOT-NAME in the object OBJECT to NEW-VALUE."
  (with-slot-definition (slot class object slot-name)
    (setf (closer-mop:slot-value-using-class class object slot) new-value)))

(defun object-boundp (object slot-name)
  "Returns true if the slot named SLOT-NAME in OBJECT is bound; otherwise, returns false."
  (with-slot-definition (slot class object slot-name)
    (closer-mop:slot-boundp-using-class class object slot)))

(defun object-makunbound (object slot-name)
  "Make the slot named SLOT-NAME in the object OBJECT unbound."
  (with-slot-definition (slot class object slot-name)
    (closer-mop:slot-makunbound-using-class class object slot)))

(defun object-computed-values (object slot-name)
  "Get the computed values of the slot named SLOT-NAME in the object OBJECT."
  (with-slot-definition (slot class object slot-name
                         :find-slot find-netfarm-computed-slot)
    (closer-mop:slot-value-using-class class object slot)))

(defun (setf object-computed-values) (new-values object slot-name)
  "Set the computed values of the slot named SLOT-NAME in the object OBJECT to NEW-VALUES."
  (with-slot-definition (slot class object slot-name
                         :find-slot find-netfarm-computed-slot)
    (setf (closer-mop:slot-value-using-class class object slot)
          new-values)))

(defgeneric compute-dependencies (object)
  (:documentation "Compute the dependencies of a vague-object, producing a list of object names, or an object, producing a list of objects.")
  (:method ((vague-object vague-object))
    (append
     (list (vague-object-schema-name vague-object))
     (let ((reference-hashes '()))
       (map-references (lambda (reference)
			 (push (reference-hash reference)
			       reference-hashes))
		       vague-object)
       reference-hashes)
     (loop for (key . nil) in (vague-object-signatures vague-object)
           collect (reference-hash key))))
  (:method ((object object))
    (append
     (list (class->schema (class-of object)))
     (loop for (keys . nil) in (object-signatures object)
           collect (keys->object keys)))))

(defun vague-object-schema-name (vague-object)
  (or (gethash "schema" (vague-object-metadata vague-object))
      (error "no schema in ~s" vague-object)))

(defun map-objects (function object)
  "Call a function with every object (including the provided object, and schemas) that would have to be saved to find the object successfully."
  (let ((seen (make-hash-table :test 'eql))
        (objects-seen 0)
        (to-visit (list object)))
    (loop until (null to-visit)
          do (let ((object (pop to-visit)))
               (unless (gethash object seen)
                 (typecase object
                   (object
                    (funcall function object)
                    (let ((class (class-of object)))
                      (map-slots
                       (lambda (name definition)
                         (declare (ignore name))
                         (when (closer-mop:slot-boundp-using-class
                                class object definition)
                           (push (closer-mop:slot-value-using-class
                                  class object definition)
                                 to-visit)))
                       class)
                      (unless (eq class (find-class 'netfarm:schema))
                        (push (netfarm:class->schema class) to-visit)))
                    (incf objects-seen))
                   (cons
                    (push (first object) to-visit)
                    (push (rest object) to-visit)))
                 (setf (gethash object seen) t))))
    objects-seen))

(defmacro do-objects ((object starting-object) &body body)
  `(map-objects (lambda (,object) ,@body)
                ,starting-object))

(defun map-references (function vague-object)
  "Call a function with every reference in vague-object."
  (let ((references-seen 0)
	(to-visit '()))
    (maphash (lambda (key value)
	       (declare (ignore key))
	       (push value to-visit))
	     (vague-object-values vague-object))
    (loop until (null to-visit)
	  do (let ((object (pop to-visit)))
	       (typecase object
		 (cons
		  (push (first object) to-visit)
		  (push (rest object) to-visit))
		 (reference
		  (funcall function object)
		  (incf references-seen)))))
    references-seen))
