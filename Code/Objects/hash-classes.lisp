(in-package :netfarm)

;;; Hash consing schema->class
(defvar *classes*
  (trivial-garbage:make-weak-hash-table :test 'equal
                                        :weakness :value
                                        :weakness-matters nil)
  "A table that maps schema hashes to Netfarm classes.")

;;; Hash consing class->schema
(defvar *schemas*
  (trivial-garbage:make-weak-hash-table :test 'eq
                                        :weakness :value
                                        :weakness-matters nil)
  "A table that maps Netfarm classes to schemas.")

(defgeneric intern-class (class)
  (:method ((class netfarm-class))
    (let ((schema (class->schema class)))
      (setf (gethash (hash-object* schema) *classes*) class
            (gethash class *schemas*)                 schema))))
