(in-package :netfarm)

;;; Some inbuilt classes.
(defclass schema ()
  ((inbuilt-name :netfarm-name nil
                 :initarg :inbuilt-name
                 :initform nil
                 :reader schema-inbuilt-name)
   (slots :initarg :slots :reader schema-slots)
   (computed-slots :initarg :computed-slots :reader schema-computed-slots)
   (documentation :initarg :documentation :reader schema-documentation)
   (presentation-script :initarg :presentation-script :reader schema-presentation-script)
   (scripts :initarg :scripts :reader schema-scripts)
   (message-script :initarg :message-script
                   :reader schema-message-script
                   :documentation "The script that is run when an instance of this schema receives a message. The first procedure in the script is called with the subject object, the object that sent the message, and the message itself as arguments."))
  (:metaclass inbuilt-netfarm-class)
  (:schema-name "inbuilt@schema"))
(let ((schema-schema (class->schema (find-class 'schema))))
  (setf (gethash (find-class 'schema) *schemas*) schema-schema))

(defclass user ()
  ((sign-key :initarg :sign-key :reader user-sign-key)
   (ecdh-key :initarg :ecdh-key :reader user-ecdh-key)
   (datum    :computed t :reader user-datum))
  (:metaclass inbuilt-netfarm-class)
  (:schema-name "inbuilt@user"))

(defclass user-map ()
  ((antitriples :initarg :antitriples
                :initform '()
                :reader user-map-antitriples)
   (triples :initarg :triples
            :initform '()
            :reader user-map-triples)
   (delegates :initarg :delegates
              :initform '()
              :reader user-map-delegates))
  (:metaclass inbuilt-netfarm-class)
  (:schema-name "inbuilt@map"))
