(in-package :netfarm)

;;; This is a very stripped down implementation of a hash table. We use this
;;; custom implementation because it should be more efficient as all keys are
;;; strings, and hashes can be computed at compile-time with a compiler macro.
;;; The main downside is that, with no resizing and lists as buckets,
;;; performance will deteriorate significantly with many keys; but we predict
;;; few classes will have enough slots to cause an issue. The other downside is
;;; that there are many unused slots, but you are using a language where the
;;; compiler and debugger are always in memory. Can you really complain about
;;; another few kilobytes per class?

(deftype string-hash-table-index ()
  '(integer 0 63))
(deftype simpleish-string ()
  'string)

(declaim (ftype (function (simpleish-string) string-hash-table-index)
                hash-string)
         (inline hash-string))

(defun hash-string (string)
  (declare (optimize (speed 3))
           #+sbcl (sb-ext:muffle-conditions sb-ext:compiler-note))
  (loop with accumulator of-type string-hash-table-index = 0
        for char of-type character across string
        do (setf accumulator
                 (ldb (byte 6 0)
                      (+ accumulator (char-code char))))
        finally (return accumulator)))

(defun make-string-hash-table ()
  (make-array 64
              :element-type 'list
              :initial-element '()))

(declaim (ftype (function ((simple-array list (*)) simpleish-string)
                          (values t boolean))
                string-gethash)
         (ftype (function ((simple-array list (*)) string-hash-table-index simpleish-string)
                          (values t boolean))
                %search-hash)
         (inline string-gethash %search-hash))

(defun %search-hash (table hash key)
  (let ((bucket (aref table hash)))
    (dolist (item bucket)
      ;; We would like to tell the Common Lisp compiler that every car of this
      ;; alist is a string, but we cannot, so it must trust us blindly when we
      ;; destructure it.
      (declare (optimize (speed 3) (safety 0))
               (cons item))
      (when (string= key (the simpleish-string (car item)))
        (return-from %search-hash (values (cdr item) t)))))
  (values nil nil))

(defun string-gethash (table key)
  (declare (optimize (speed 3) (debug 1))
           #+sbcl (sb-ext:muffle-conditions sb-ext:compiler-note))
  (%search-hash table (hash-string key) key))

(define-compiler-macro string-gethash (&whole whole table key)
  (if (stringp key)
      `(%search-hash ,table ,(hash-string key) ,key)
      whole))

(declaim (ftype (function (t (simple-array list (*)) simpleish-string) t)
                (setf string-gethash))
         (inline (setf string-gethash)))
(defun (setf string-gethash) (value table key)
  #+sbcl (declare (sb-ext:muffle-conditions sb-ext:compiler-note))
  (push (cons (copy-seq key) value)
        (aref table (hash-string key)))
  t)

(defun string-hash-table->alist (table)
  (loop for bucket across table appending bucket))
