(in-package :netfarm)

;;; Inbuilt objects (not classes)
(defclass named-mixin ()
  ((inbuilt-name :initarg :inbuilt-name :type string :reader named-mixin-name)))

(defun make-named-instance (class name &rest initargs)
  (apply #'make-instance
         (make-instance 'standard-class
                        :direct-superclasses (list class (find-class 'named-mixin)))
         :inbuilt-name name
         initargs))                                        

(defvar *inbuilt-objects*
  (trivial-garbage:make-weak-hash-table :test 'equal)
  "A table that maps names to inbuilt objects.")

(defmethod initialize-instance :after ((object named-mixin) &key)
  (setf (gethash (named-mixin-name object) *inbuilt-objects*)
        object))

(defun inbuilt-object (hash)
  "Returns the inbuilt object with given hash, or returns NIL if there isn't such an object."
  (values (gethash hash *inbuilt-objects* nil)))

;;; Inbuilt classes
(defvar *inbuilt-classes* (make-hash-table :test 'equal)
  "A table that maps names to inbuilt classes.")

(defclass inbuilt-netfarm-class (netfarm-class)
  ((schema-names :initarg :schema-name :reader inbuilt-netfarm-class-schema-names))
  (:documentation "A class for definining inbuilt Netfarm classes (which have names that are not hash names). 
Don't ever ever ever use this outside of the Netfarm system, or you will create inconsistent state w.r.t the rest of the world and no one will like you."))

(defmethod initialize-instance :after ((class inbuilt-netfarm-class) &key)
  (assert (= (length (inbuilt-netfarm-class-schema-names class)) 1)))

(defmethod intern-class ((class inbuilt-netfarm-class))
  (setf (gethash (netfarm-class-name class) *inbuilt-classes*) class)
  (unless (eql (class-name class) 'schema)
    ;; We can't create the schema of the schema until the class has been bound.
    (let ((schema (class->schema class)))
      (setf (gethash class *schemas*) schema))))

(defmethod netfarm-class-name ((class inbuilt-netfarm-class))
  (first (inbuilt-netfarm-class-schema-names class)))
