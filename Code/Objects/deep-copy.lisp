(in-package :netfarm)

(defvar *slot-blacklist* '(signatures source)
  "Slots which should not be deep-copied.")
(defvar *shallow-slots*  '(source)
  "Slots which should be shallow-copied. This should be a subset of *SLOT-BLACKLIST*.")

(defgeneric %deep-copy-object (object callback)
  (:method (garbage callback)
    (warn "Can't copy ~s" garbage)
    (funcall callback garbage)
    '())
  (:method ((hash-table hash-table) callback)
    ;; This isn't Netfarm data, but is used for metadata tables, so we have
    ;; to be able to copy those too.
    (let ((new-table (alexandria:copy-hash-table hash-table)))
      (funcall callback new-table)
      (loop for key   being the hash-keys   of new-table
            for value being the hash-values of new-table
            collect (cons value
                          (lambda (value)
                            (setf (gethash key new-table) value))))))
  (:method ((class class) callback)
    (funcall callback class)
    '())
  (:method ((object object) callback)
    (let* ((class (class-of object))
           (new-object (allocate-instance class)))
      (flet ((update-slot (slot)
               (lambda (slot-value)
                 (setf (closer-mop:slot-value-using-class class new-object
                                                          slot)
                       slot-value))))
        (funcall callback new-object)
        ;; Shallow copy the shallow slots.
        (loop for slot-name in *shallow-slots*
              when (slot-boundp object slot-name)
                do (setf (slot-value new-object slot-name)
                         (slot-value object     slot-name)))
        ;; Queue the non-blacklisted bound slots for copying.
        (loop for slot in (closer-mop:class-slots class)
              when (and (closer-mop:slot-boundp-using-class class object slot)
                        (not (member (closer-mop:slot-definition-name slot)
                                     *slot-blacklist*)))
                collect (cons (closer-mop:slot-value-using-class class
                                                                 object
                                                                 slot)
                              (update-slot slot))))))
  (:method ((number number) callback)
    (funcall callback number)
    '())
  (:method ((cons cons) callback)
    (let ((new-cons (cons nil nil)))
      (funcall callback new-cons)
      (list (cons (car cons)
                  (lambda (car)
                    (setf (car new-cons) car)))
            (cons (cdr cons)
                  (lambda (cdr)
                    (setf (cdr new-cons) cdr))))))
  (:method ((vector vector) callback)
    ;; This should be a vector of (unsigned-byte 8), which we don't copy.
    (funcall callback (alexandria:copy-array vector))
    '())
  (:method ((null null) callback)
    (funcall callback '())
    '()))

(defun deep-copy-object (object)
  ;; This deep copying algorithm uses an auxiliary stack that is a list of
  ;; conses, each with an object to copy in the CAR and a function to call
  ;; with the shallow-copied object in the CDR.
  (loop with copied-object = nil
        with cache = (make-hash-table :test 'eql)
        with stack = (list
                      (cons object
                            (lambda (new-object)
                              (setf copied-object new-object))))
        until (null stack)
        do (destructuring-bind (object-to-copy . callback)
               (pop stack)
             (multiple-value-bind (already-copied-object present?)
                 (gethash object-to-copy cache)
               (if present?
                   (funcall callback already-copied-object)
                   (setf stack (append (%deep-copy-object object-to-copy callback)
                                       stack)))))
        finally (return-from deep-copy-object copied-object)))
