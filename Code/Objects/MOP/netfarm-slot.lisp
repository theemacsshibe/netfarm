(in-package :netfarm)

(defclass netfarm-slot (closer-mop:standard-direct-slot-definition)
  ((computed :initarg :computed :initform nil)
   (documentation :initarg :documentation
                  :reader slot-documentation)
   (netfarm-name :initarg :netfarm-name
                 :reader slot-netfarm-name)))
(defmethod print-object ((slot netfarm-slot) stream)
  (print-unreadable-object (slot stream :type t)
    (format stream "~s" (slot-netfarm-name slot))))

(defclass netfarm-computed-slot (netfarm-slot)
  ())

(defmethod initialize-instance :around ((slot netfarm-computed-slot)
                                        &rest rest
                                        &key initform name)
  (unless (null initform)
    (error "Cannot supply an initform for the computed slot ~s"
           name))
  (apply #'call-next-method slot
         :initform '()
         :initfunction (constantly '())
         rest))

(defmethod initialize-instance :after ((slot netfarm-slot) &key)
  ;; Generate a netfarm-slot name from the slot's symbol-name.
  (unless (slot-boundp slot 'netfarm-name)
    (setf (slot-value slot 'netfarm-name)
          (string-downcase (closer-mop:slot-definition-name slot)))))

(defmethod closer-mop:direct-slot-definition-class ((class netfarm-class) &rest initargs)
  (declare (ignore class))
  (destructuring-bind (&key computed &allow-other-keys) initargs
    (if computed
        (find-class 'netfarm-computed-slot)
        (find-class 'netfarm-slot))))

(declaim (inline find-netfarm-slot map-slots
                 find-netfarm-computed-slot map-computed-slots))
(defun find-netfarm-slot (class slot-name)
  "Find the effective slot definition for a Netfarm slot."
  (etypecase slot-name
    (symbol (setf slot-name (string-downcase slot-name)))
    (string))
  (string-gethash (netfarm-class-slot-table class) slot-name))

(defun map-slots (function class)
  "Call the function with each slot name and effective slot definition in the class."
  (let ((table (netfarm-class-slot-table class)))
    (loop for bucket across table
          do (loop for (name . definition) in bucket
                   do (funcall function name definition)))))

(defun find-netfarm-computed-slot (class slot-name)
  "Find the effective slot definition for a Netfarm computed slot."
  (etypecase slot-name
    (symbol (setf slot-name (string-downcase slot-name)))
    (string))
  (string-gethash (netfarm-class-computed-slot-table class) slot-name))

(defun map-computed-slots (function class)
  "Call the function with each computed slot name and definition in the class."
  (let ((table (netfarm-class-computed-slot-table class)))
    (loop for bucket across table
          do (loop for (name . definition) in bucket
                   do (funcall function name definition)))))

