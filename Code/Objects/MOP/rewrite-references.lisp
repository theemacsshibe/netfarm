(in-package :netfarm)

(defclass rewriting-metaclass-mixin ()
  ()
  (:documentation "A metaclass mixin that \"rewrites\" its classes' instances' slots, replacing all references R with (funcall (object-source <instance>) (reference-hash R))."))

(defclass rewriting-information-mixin ()
  ((source :initarg :source :initform nil :accessor object-source)))

(defvar *rewrite-references?* t
  "Should we rewrite references?")

(defun overwrite-instance (target source)
  "Overwrite an object TARGET by changing its class to (CLASS-OF SOURCE), and then binding its slots to those of SOURCE."
  (let ((source-class (class-of source)))
    (change-class target source-class)
    (dolist (slot (closer-mop:class-slots source-class))
      (when (closer-mop:slot-boundp-using-class source-class source slot)
        (setf (closer-mop:slot-value-using-class source-class target slot)
              (closer-mop:slot-value-using-class source-class source slot))))
    (initialize-instance target)))

(defun find-schema-in-table (hash table)
  (alexandria:if-let ((class (gethash hash table)))
    (class->schema class)))
(defun reference-replacement (hash function)
  "Find an object with a given hash, possibly calling the function if the object is not known internally."
  (or (gethash hash *inbuilt-objects*)
      (find-schema-in-table hash *inbuilt-classes*)
      (find-schema-in-table hash *classes*)
      (funcall function hash)))

(defun rewrite-graph (graph object-source-function)
  (let ((visited (make-hash-table))
        (to-visit (list graph)))
    (loop until (null to-visit)
          do (let ((object (pop to-visit)))
               (setf (gethash object visited) t)
               (typecase object
                 (cons
                  (push (car object) to-visit)
                  (push (cdr object) to-visit))
                 (reference
                  (assert (stringp (reference-hash object)))
                  (overwrite-instance object
                                      (reference-replacement (reference-hash object)
                                                             object-source-function))
                  (initialize-instance object))
                 (hash-table
                  (maphash (lambda (key value)
                             (declare (ignore key))
                             (push value to-visit))
                           object)))))))

(declaim (inline maybe-rewrite-graph))
(defun maybe-rewrite-graph (object graph-cons)
  (when (and (not (car graph-cons))
             *rewrite-references?*)
    (unless (null (object-source object))
      (rewrite-graph (cdr graph-cons) (object-source object)))
    (setf (car graph-cons) t))
  (cdr graph-cons))

;;; Methods to automagically rewrite object graphs in slots to remove references.
(defmethod closer-mop:slot-value-using-class :around
    ((class rewriting-metaclass-mixin) object slot)
  (if (eq (closer-mop:slot-definition-name slot) 'source)
      (call-next-method)
      (maybe-rewrite-graph object (call-next-method))))

(defmethod (setf closer-mop:slot-value-using-class) :around
    (new-value (class rewriting-metaclass-mixin) object slot)
  (if (eq (closer-mop:slot-definition-name slot) 'source)
      (call-next-method)
      (call-next-method (cons nil new-value)
                        class object slot)))
