(in-package :netfarm)

(defgeneric netfarm-class-scripts (class)
  (:method ((class standard-class)) '()))
(defgeneric netfarm-class-message-script (class)
  (:method ((class standard-class)) nil))
(defgeneric netfarm-class-presentation-script (class)
  (:method ((class standard-class)) nil))

(defclass object-metaclass (rewriting-metaclass-mixin standard-class)
  ())

(defmethod closer-mop:validate-superclass ((class object-metaclass)
                                           (standard-class standard-class))
  t)

(defclass netfarm-class (object-metaclass)
  ((presentation-script :initform nil :initarg
                        :presentation-script
                        :reader netfarm-class-presentation-script)
   (direct-scripts :initform '()
                   :initarg :scripts
                   :reader netfarm-class-direct-scripts)
   (scripts :reader netfarm-class-scripts)
   (message-script :initform nil
                   :initarg :message-script
                   :reader netfarm-class-message-script)
   (slot-table :reader netfarm-class-slot-table)
   (computed-slot-table :reader netfarm-class-computed-slot-table))
  (:documentation "The metaclass of a class Netfarm knows how to serialise, take OBJECT-VALUE of, and create instance of, as well as a class that Netfarm can get scripts from.
Providing the class option (:scripts script ...) will attach the initialisation scripts to the class, and providing the class option (:message-script script) will attach the message script to the class. Both are evaluated, but not in the lexical environment of the DEFCLASS form because those are not evaluated by DEFCLASS, and we have to evaluate them with EVAL ourselves."))

(defun force-script-data-evaluation (class)
  "Evaluate the provided forms that designate scripts, and merge them with the superclasses' scripts."
  (setf (slot-value class 'direct-scripts)
	(mapcar #'eval (netfarm-class-direct-scripts class))
        ;; The (singular) presentation script is inherited from the first
        ;; superclass that has a presentation script.
        (slot-value class 'presentation-script)
        (if (null (netfarm-class-presentation-script class))
            nil
            (destructuring-bind (script)
                (netfarm-class-presentation-script class)
              (eval script)))
        ;; The (singular) message script is inherited from the first superclass
        ;; that has a message script.
        (slot-value class 'message-script)
        (if (null (netfarm-class-message-script class))
            (some #'netfarm-class-message-script
                  (closer-mop:class-direct-superclasses class))
            (destructuring-bind (script)
                (netfarm-class-message-script class)
              (eval script)))
        ;; The initialisation scripts of a class are the direct scripts of itself
        ;; and the scripts of its superscripts, with duplicates removed. This isn't
	;; very different to how slots are inherited in CLOS.
        (slot-value class 'scripts)
	(remove-duplicates
	 (append (netfarm-class-direct-scripts class)
		 (loop for class in (closer-mop:class-direct-superclasses class)
		       appending (netfarm-class-scripts class))))))

(defmethod initialize-instance :after ((class netfarm-class) &key)
  (force-script-data-evaluation class)
  (intern-class class))

(defmethod reinitialize-instance :after ((class netfarm-class) &key)
  (force-script-data-evaluation class))

(defmethod netfarm-class-slot-table :before ((class netfarm-class))
  (unless (closer-mop:class-finalized-p class)
    (closer-mop:finalize-inheritance class)))
(defmethod netfarm-class-computed-slot-table :before ((class netfarm-class))
  (unless (closer-mop:class-finalized-p class)
    (closer-mop:finalize-inheritance class)))

(defgeneric netfarm-class-name (class)
  (:method ((class netfarm-class))
    (hash-object* (class->schema class))))

(defclass object (rewriting-information-mixin)
  ((metadata :initarg :metadata :initform (make-hash-table :test 'equal)
             :reader object-metadata)
   (signatures :initarg :signatures :initform nil :accessor object-signatures))
  (:metaclass object-metaclass))

(defmethod closer-mop:class-direct-superclasses ((class netfarm-class))
  (append (remove (find-class 'standard-object) (call-next-method))
          (list (find-class 'object)
                (find-class 'standard-object))))
  
(defgeneric netfarm-slots (class)
  (:method (class)
    (declare (ignore class))
    '())
  (:method ((class netfarm-class))
    (append (loop for slot in (closer-mop:class-direct-slots class)
                  when (typep slot 'netfarm-slot)
                    collect slot)
            (loop for superclass in (closer-mop:class-direct-superclasses class)
                  append (netfarm-slots superclass)))))

(defun find-effective-slot-definition (class name)
  (find name
        (closer-mop:class-slots class)
        :key #'closer-mop:slot-definition-name))

(defmethod closer-mop:finalize-inheritance :after ((class netfarm-class))
  "Populate the slot and computed-slot tables."
  (let ((slot-table (make-string-hash-table))
        (computed-slot-table (make-string-hash-table)))
    (labels ((maybe-add-class-slots (class)
               (when (typep class 'netfarm-class)
                 (dolist (slot (closer-mop:class-direct-slots class))
                   (maybe-add-slot slot))))
             (maybe-add-slot (slot)
               (let ((name (slot-netfarm-name slot)))
                 (unless (null name)
                   (if (typep slot 'netfarm-computed-slot)
                       (add-computed-slot slot name)
                       (add-slot slot name)))))
             (add-computed-slot (slot name)
               (if (string-gethash computed-slot-table name)
                   (error "duplicate computed slot name: ~s" name)
                   (setf (string-gethash computed-slot-table name)
                         (find-effective-slot-definition
                          class
                          (closer-mop:slot-definition-name slot)))))
             (add-slot (slot name)
               (if (string-gethash slot-table name)
                   (error "duplicate slot name: ~s" name)
                   (setf (string-gethash slot-table name)
                         (find-effective-slot-definition
                          class
                          (closer-mop:slot-definition-name slot))))))
      (dolist (class (closer-mop:class-precedence-list class))
        (maybe-add-class-slots class))
      (setf (slot-value class 'slot-table) slot-table
            (slot-value class 'computed-slot-table) computed-slot-table))))
