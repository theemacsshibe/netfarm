(in-package :netfarm)

(define-tuple computed-value ()
  ((cause) (value))
  "A computed value that was added to an object by another object.")

(defgeneric add-computed-value (object cause name value)
  (:method ((object object) cause name value)
    (push (make-instance 'computed-value
                         :cause cause
                         :value value)
          (object-computed-values object name))
    t))
