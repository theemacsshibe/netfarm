(in-package :netfarm)

;;; These are some macros I thought would be handy over time.
;;; So far define-tuple is only used, but they're all very nice
;;; and useful.

(defmacro never-null-let* (lets &body body)
  "This is a strange form of anaphora, similar to ANAPHORA-BASIC:AAND).
When one bound form evaluates to NIL, the chain is discarded and
evaluation stops. The body is also not evaluated, that'd be weird.
For example, (never-null-let* ((a (print :a)) (b nil) (c (print :c))))
will print :A but not :C. Variables are also bound in subsequent
computations, hence the name \"never-null-let*\". Enough explanation?"
  (let ((failed (gensym "FAILED")))
    `(catch ',failed
       (let* ,(loop for (var bind) in lets
                 collect `(,var (or ,bind
                                    (throw ',failed nil))))
         ,@body))))

(defmacro case* (value (&key (test #'eql)
                             fall-through)
                 &body cases)
  ;; This macro just cleans up some lookup tables.
  "CASE, but you get to choose the equality predicate.
No lookup tables or constant folding/pointer magic though."
  (let ((actual-value (gensym "VALUE"))
        (actual-test (gensym "TEST")))
    `(let ((,actual-value ,value)
           (,actual-test ,test))
       (cond
         ,@(loop for (test-value . body) in cases
              collect `((funcall ,actual-test ,test-value ,actual-value)
                        ,@body))
         (t ,fall-through)))))

(defmacro define-tuple (class-name parents slots &optional documentation)
  "This is just to save my hands, since Netfarm class slots are mostly disjoint
and have the same printer basically.
CLASS-NAME is the name of the slot, PARENTS should be called 
DIRECT-SUPERCLASSES to be accurate, SLOTS are a modified slot description list,
and DOCUMENTATION is the docstring.

SLOTS takes the form (name . plist), where plist contains:
- :READER, the reader/setter of the slot (defaults to CLASS-SLOT like DEFSTRUCT),
- :READ-ONLY, truthy if the slot should be read-only,
- :INITARG, the keyword argument,
- :INITFORM, the default value.

This generates the DEFCLASS form, and a method on PRINT-OBJECT."
  (let ((not-found (gensym "NOT-FOUND"))
        (slot-alist nil))
    (flet ((getf-or-default (list value &optional default)
             (let ((value (getf list value not-found)))
               (if (eql value not-found)
                   default
                   value))))
      `(progn
         (defclass ,class-name ,parents
           ,(loop
               for (name . arguments) in slots
               for reader-name = (or (getf arguments :reader)
                                     (intern (format nil "~a-~a" class-name name)))
               do (push (cons name reader-name) slot-alist)
               collect `(,name :initarg  ,(getf-or-default arguments :initarg
                                                           (intern (symbol-name name) 'keyword))
                               :initform ,(getf-or-default arguments :initform
                                                           `(error
                                                             ,(format nil "~a is required" name)))
                               ,(if (getf arguments :read-only)
                                    :reader :accessor)
                               ,reader-name))
           ,@(unless (null documentation)
               (list (list :documentation documentation))))
         (defmethod print-object ((object ,class-name) stream)
           (print-unreadable-object (object stream :type t :identity t)
             (pprint-logical-block (stream nil)
             ,@(loop for (name . reader) in (reverse slot-alist)
                     collect `(progn
                                (write-char #\: stream)
                                (write-string ,(symbol-name name) stream)
                                (write-char #\Space stream)
                                (pprint-newline :miser stream)
                                (prin1 (,reader object) stream)
                                (write-char #\Space stream)
                                (pprint-newline :linear stream))))))))))

(defmacro aif (test then &optional else)
  `(let ((it ,test))
     (if it ,then ,else)))

